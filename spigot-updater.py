import re, urllib2, time, os, hashlib, sys

os.chdir(os.path.dirname(os.path.realpath(__file__)))

print "To run this program you need build 1649 of spigot, and spigot patcher"
print "Change their names in updater below if you want"

build1649="spigot-1.7.10-R0.1-SNAPSHOT.jar"
patcher="SpigotPatcher.jar"

if not os.path.isfile(patcher) or not os.path.isfile(build1649):
  print "Patcher or spigot's 1649 build not found - exiting program"
  sys.exit()

opener = urllib2.build_opener()
opener.addheaders = [("User-agent", "Mozilla/5.0 (compatible; SpigotPatchUpdater/0.1)")]
urllib2.install_opener(opener)

html = urllib2.urlopen('http://www.spigotmc.org/spigot-updates/').read()

spigot = map(lambda s: s.split('</a>                               '), re.findall('>spigot-.*?\.bps<\/a>.*?\d\d-\D\D\D-\d\d\d\d \d\d:\d\d',html))
spigot = map(lambda s: [s[0][1:], time.strptime(s[1], '%d-%b-%Y %H:%M')], spigot)
spigot = sorted(spigot, key=lambda k: k[1])
spigot = spigot[len(spigot)-1][0]

print "Lastest version of patch for spigot is \""+spigot+'".'
if os.path.isfile(spigot):
  print "You have already downloaded the lastest version."
else:
  print "You do not have it yet - download started."
  patch = urllib2.urlopen('http://www.spigotmc.org/spigot-updates/'+spigot).read()
  print "Patch downloaded, checking MD5."
  md5 = urllib2.urlopen('http://www.spigotmc.org/spigot-updates/'+spigot.split('.')[0]+'.md5').read()
  if hashlib.md5(patch).hexdigest() == md5.split("\n")[1].split(' ')[1]:
    print "Correct MD5, saving patch to file."
    file = open(spigot,'wb')
    file.write(patch)
    file.close()
    print "Started patching spigot to version \""+spigot.split('.')[0]+'"'
    os.system("java -jar "+patcher+" "+build1649+" "+spigot+" "+spigot.split('.')[0]+".jar")
    print "Patching finished"
    if os.path.isfile(spigot.split('.')[0]+".jar"):
      print "Patched spigot found - checking md5"
      if hashlib.md5(open(spigot.split('.')[0]+".jar", 'rb').read()).hexdigest() == md5.split("\n")[2].split(' ')[1]:
        print "MD5 of patched spigot is correct :)"
        try:
          os.remove("spigot-lastest.jar")
        except OSError:
          pass
        if sys.platform != "win32":
          os.symlink(spigot.split('.')[0]+".jar", "spigot-lastest.jar")
        else:
          open('spigot-lastest.jar','wb').write(open(spigot.split('.')[0]+'.jar','rb').read())
        sys.exit()
      else:
        print hashlib.md5(open(spigot.split('.')[0]+".jar", 'rb').read()).hexdigest()
        print "MD5 of patched spigot is wrong :C"
        sys.exit()
  else:
    print "Wrong MD5 :C"
    sys.exit()